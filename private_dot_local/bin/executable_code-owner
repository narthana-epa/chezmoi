#!/usr/bin/env ruby

def read_codeowners_file
  # Assuming the CODEOWNERS file is located at the root of the repository
  codeowners_path = ".github/CODEOWNERS"

  if File.exist?(codeowners_path)
    return File.read(codeowners_path)
  else
    puts "CODEOWNERS file does not exist."
    exit(1)
  end
end

def find_owner(codeowners_content, file_path)
  codeowners_lines = codeowners_content.split("\n")
  owners = []

  codeowners_lines.each do |line|
    next if line.strip.empty? || line.strip[0] == '#'

    pattern_and_owners = line.split(/\s+/, 2)
    pattern = pattern_and_owners[0]
    owner = pattern_and_owners[1].gsub(/@([a-zA-Z0-9\-]+)/) { $1 }

    if file_path.end_with?(pattern)
      owners << owner
    end
  end

  return owners.join(", ")
end


codeowners_content = read_codeowners_file

unless codeowners_content
  puts "Unable to determine the owner(s)."
  exit(1)
end

file_paths = ARGV.length > 0 ? ARGV : STDIN.read.split("\n")

file_paths.each do |file_path|
  owner = find_owner(codeowners_content, file_path)
  puts "The owner(s) of #{file_path} is/are: #{owner}"
end
