# shellcheck shell=bash

set -Eeufo pipefail

# find the branch name if the commit is attached to a branch
LOCAL_BRANCH=$(git symbolic-ref --short HEAD 2>/dev/null || echo "detached")

# find the remote name of the branch or return the first remote in the config
REMOTE=$(git config "branch.$LOCAL_BRANCH.remote" || git config --list | rg -m1 '^remote\.' | awk -F. '{ print $2 }')

# find the remote's trunk branch name
git symbolic-ref "refs/remotes/$REMOTE/HEAD" | sd "^refs/remotes/$REMOTE/" '' >/dev/null || git remote set-head origin -a >/dev/null 2>&1
TRUNK_BRANCH=$(git symbolic-ref "refs/remotes/$REMOTE/HEAD" | sd "^refs/remotes/$REMOTE/" '')
