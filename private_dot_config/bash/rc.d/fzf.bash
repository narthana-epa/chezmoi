# vim: ft=sh:

# FZF options
export FZF_DEFAULT_OPS="--extended"
export FZF_FIND_FILE_COMMAND="fd --exclude 'Dropbox' --follow"
export FZF_DEFAULT_COMMAND='rg --files --hidden --follow --glob "!.git/*"'
