# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

include /etc/sway/config.d/*
include config.d/*

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left  h
set $down  j
set $up    k
set $right l
# Preferred terminal emulator
set $term alacritty --working-directory "$(swaycwd)"
# Preferred application launcher
set $menu rofi -show drun -run-shell-command '{terminal} -e fish -ic "{cmd} && read"'

workspace_layout tabbed

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
# output * bg /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_1920x1080.png fill
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Input configuration
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

input type:touchpad {
    dwt enabled
    tap enabled
    natural_scroll enabled
    middle_emulation enabled
}

input type:keyboard {
    xkb_layout us
    xkb_numlock enable
    xkb_options caps:escape,ctrl:swap_lalt_lctl
}

### Key bindings
#
# Basics:
#
# start a terminal
bindsym $mod+Return exec $term

# kill focused window
bindsym $mod+Shift+q kill

# start your launcher
bindsym $mod+d exec $menu

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal

# reload the configuration file
bindsym $mod+Shift+c reload

# restart sway in place
bindsym $mod+Shift+r restart

# exit sway (logs you out of your wayland session)
bindsym $mod+Shift+e exit
#
# Moving around:
#
# Move your focus around
bindsym $mod+$left  focus left
bindsym $mod+$down  focus down
bindsym $mod+$up    focus up
bindsym $mod+$right focus right
# or use $mod+[up|down|left|right]
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Up    focus up
bindsym $mod+Right focus right

# _move_ the focused window with the same, but add Shift
bindsym $mod+Shift+$left  move left
bindsym $mod+Shift+$down  move down
bindsym $mod+Shift+$up    move up
bindsym $mod+Shift+$right move right
# ditto, with arrow keys
bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Right move right
#
# Workspaces:
#
# Note: workspaces can have any name you want, not just numbers.
# We just use 1-10 as the default.
#
# switch to workspace
bindsym $mod+1 workspace number 1
bindsym $mod+2 workspace number 2
bindsym $mod+3 workspace number 3
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7
bindsym $mod+8 workspace number 8
bindsym $mod+9 workspace number 9
bindsym $mod+0 workspace number 10
# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10

# move workspace to new monitor
bindsym $mod+Shift+Mod1+$left  move workspace to output left
bindsym $mod+Shift+Mod1+$down  move workspace to output down
bindsym $mod+Shift+Mod1+$up    move workspace to output up
bindsym $mod+Shift+Mod1+$right move workspace to output right

# Layout stuff:
#
# Spilts
bindsym $mod+backslash split horizontal
bindsym $mod+minus     split vertical

# Switch the current container between different layout styles
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+Shift+f floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+Mod1+f focus mode_toggle

# move focus to the parent container
bindsym $mod+a focus parent

# Screenshots
bindsym Print            exec grimshot --notify save active
bindsym Mod4+Print       exec grimshot --notify save area
bindsym Mod4+Mod1+Print  exec grimshot --notify save output
bindsym Mod4+Shift+Print exec grimshot --notify save window

## move focus to the child container
#bindsym $mod+c focus child

#
# Scratchpad:
#
# Sway has a "scratchpad", which is a bag of holding for windows.
# You can send windows there and get them back later.

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+m move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+m scratchpad show

#
# Resizing containers:
#
mode "resize" {
    bindsym $left  resize shrink width  10 px or 10 ppt
    bindsym $down  resize grow   height 10 px or 10 ppt
    bindsym $up    resize shrink height 10 px or 10 ppt
    bindsym $right resize grow   width  10 px or 10 ppt

    # ditto, with arrow keys
    bindsym Left  resize shrink width  10 px or 10 ppt
    bindsym Down  resize grow   height 10 px or 10 ppt
    bindsym Up    resize shrink height 10 px or 10 ppt
    bindsym Right resize grow   width  10 px or 10 ppt

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

#
# Status Bar:
#
bar {
    swaybar_command waybar
    position top
}

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Fira 8

####################################################

## autostarts
exec swaync
exec blueman-applet
exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
exec wl-paste -t text --watch clipman store --no-persist
exec kanshi
exec swayidle -w timeout 1800 'swaylock'
exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec sworkstyle &> /tmp/sworkstyle.log
exec avizo-service

bindsym $mod+v       exec clipman pick -t rofi
bindsym $mod+Shift+v exec clipman clear -t rofi
bindsym $mod+Shift+p exec clipman clear --all
bindsym $mod+Shift+n exec swaync-client -t -sw
bindsym $mod+Shift+i exec rofimoji

# Float by type
for_window [window_role="pop-up"]      floating enable
for_window [window_role="task_dialog"] floating enable
for_window [window_type="dialog"]      floating enable
for_window [window_type="menu"]        floating enable

# Sharing indicators
for_window [title="Firefox Developer Edition — Sharing Indicator"] floating enable; border none; move position 50 ppt 95 ppt
for_window [title="Firefox — Sharing Indicator"]                   floating enable; border none; move position 50 ppt 95 ppt

for_window [title="Picture-in-Picture"]                            floating enable; border none

# For pop up notification windows that don't use notifications api
for_window [app_id="zoom" title="^zoom$"] border none, floating enable
# For specific Zoom windows
for_window [app_id="zoom" title="^(Zoom|About)$"] border pixel, floating enable
for_window [app_id="zoom" title="Settings"] floating enable, floating_minimum_size 960 x 700

## Custom keys
bindsym XF86AudioRaiseVolume exec volumectl -u up
bindsym XF86AudioLowerVolume exec volumectl -u down
bindsym XF86AudioMute        exec volumectl toggle-mute
bindsym XF86AudioMicMute     exec volumectl -m toggle-mute

bindsym XF86MonBrightnessUp   exec lightctl up
bindsym XF86MonBrightnessDown exec lightctl down

# lock
set $locker swaylock
set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (p) poweroff
mode "$mode_system" {
    bindsym l exec $locker,                        mode "default"
    bindsym e exec swaymsg exit,                   mode "default"
    bindsym s exec systemctl suspend && $locker,   mode "default"
    bindsym h exec systemctl hibernate && $locker, mode "default"
    bindsym r exec systemctl reboot,               mode "default"
    bindsym p exec systemctl poweroff -i,          mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Alt+Shift+q mode "$mode_system"
