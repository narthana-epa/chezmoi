if _G.StatusColumn then
	return
end

RelativeModes = {
	["n"] = true,
	["no"] = true,
	["nov"] = true,
	["noV"] = true,
	["no\22"] = true,
	["niI"] = true,
	["niR"] = true,
	["niV"] = true,
	["nt"] = true,
	["ntT"] = true,
	["v"] = true,
	["vs"] = true,
	["V"] = true,
	["Vs"] = true,
	["\22"] = true,
	["\22s"] = true,
	["s"] = true,
	["S"] = true,
	["\19"] = true,
}

_G.StatusColumn = {
	handler = {
		fold = function()
			local lnum = vim.fn.getmousepos().line

			-- Only lines with a mark should be clickable
			if vim.fn.foldlevel(lnum) <= vim.fn.foldlevel(lnum - 1) then
				return
			end

			local state
			if vim.fn.foldclosed(lnum) == -1 then
				state = "close"
			else
				state = "open"
			end

			vim.cmd.execute("'" .. lnum .. "fold" .. state .. "'")
		end,
	},

	display = {
		line = function()
			local lnum = tostring(vim.v.lnum)

			if not vim.wo.relativenumber then
				return lnum
			end

			local mode = vim.api.nvim_get_mode().mode
			if RelativeModes[mode] then
				lnum = tostring(vim.v.relnum ~= 0 and vim.v.relnum or vim.v.lnum or 0)
				local num_lines = tostring(vim.api.nvim_buf_line_count(0))
				local padding = string.rep(" ", #num_lines - #lnum)
				lnum = padding .. lnum
			end

			if vim.v.wrap then
				return " " .. string.rep(" ", #lnum)
			end

			return lnum
		end,

		fold = function()
			if vim.v.wrap then
				return ""
			end

			local lnum = vim.v.lnum
			local icon = " "

			-- Line isn't in folding range
			if vim.fn.foldlevel(lnum) <= 0 then
				return icon
			end

			-- Not the first line of folding range
			if vim.fn.foldlevel(lnum) <= vim.fn.foldlevel(lnum - 1) then
				return icon
			end

			if vim.fn.foldclosed(lnum) == -1 then
				icon = "➡️"
			else
				icon = "⬇️"
			end

			return icon
		end,
	},

	sections = {
		sign_column = {
			[[%s]],
		},
		line_number = {
			[[%=%{v:lua.StatusColumn.display.line()}]],
		},
		spacing = {
			[[]],
		},
		folds = {
			[[%#FoldColumn#]], -- HL
			[[%@v:lua.StatusColumn.handler.fold@]],
			[[%{v:lua.StatusColumn.display.fold()}]],
		},
		border = {
			[[%#StatusColumnBorder#]], -- HL
			[[▐]],
		},
		padding = {
			[[%#StatusColumnBuffer#]], -- HL
			[[]],
		},
	},

	build = function(tbl)
		local statuscolumn = {}

		for _, value in ipairs(tbl) do
			if type(value) == "string" then
				table.insert(statuscolumn, value)
			elseif type(value) == "table" then
				table.insert(statuscolumn, StatusColumn.build(value))
			end
		end

		return table.concat(statuscolumn)
	end,

	set_window = function()
		vim.defer_fn(function()
			local winid = vim.api.nvim_get_current_win()
			local is_floating = vim.api.nvim_win_get_config(winid).relative ~= ""

			if not is_floating then
				local statuscolumn = StatusColumn.build({
					StatusColumn.sections.sign_column,
					StatusColumn.sections.line_number,
					StatusColumn.sections.folds,
					StatusColumn.sections.border,
				})
				vim.api.nvim_win_set_option(winid, "statuscolumn", statuscolumn)
			end
		end, 1)
	end,
}

vim.wo.relativenumber = true

local grp = vim.api.nvim_create_augroup("statuscolumn", {})
vim.api.nvim_create_autocmd({ "BufNew", "CursorMoved", "CursorMovedI" }, {
	pattern = "*",
	group = grp,
	callback = function()
		StatusColumn.set_window()
	end,
})
