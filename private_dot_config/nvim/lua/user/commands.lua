REG_CHARS = {
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"k",
	"l",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"u",
	"v",
	"w",
	"x",
	"y",
	"z",
}

vim.api.nvim_create_user_command("ClearRegisters", function()
	for _, char in ipairs(REG_CHARS) do
		vim.fn.setreg(char, "")
	end
end, {
	nargs = "*",
	desc = "Create a scratch buffer",
})
