local function t(str)
	return vim.api.nvim_replace_termcodes(str, true, true, true)
end

function _G.smart_tab()
	return vim.fn.pumvisible() == 1 and t("<C-n>") or t("<Tab>")
end

local m = vim.api.nvim_set_keymap
local opts = { expr = true, noremap = true }
m("i", "<Tab>", "v:lua.smart_tab()", opts)
