local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.g.neovide_remember_window_size = false

-- Upstream byte compilation
vim.loader.enable()

require("lazy").setup("user.plugins", {
	performance = {
		rtp = {
			disabled_plugins = {
				"gzip",
				"matchit",
				"matchparen",
				"netrwPlugin",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
})
require("user.statuscolumn")
require("user.filetype")
require("user.autocmd")
require("user.term_codes")
require("user.scratch")
require("user.commands")
require("user.general")
require("user.globals")
require("user.mappings")
require("user.trim_whitespace")
