# git clone and change directory
function git-ccd
    function extract_dir
        string match --regex '/(?P<dir>[^/]+?)(\.git)?$' $argv[1] >/dev/null
        echo $dir
    end

    set -l dir $argv[2]

    if not set -q $argv[2]
        set dir (extract_dir $argv[1])
    end

    git clone $argv
    and cd $dir
end
