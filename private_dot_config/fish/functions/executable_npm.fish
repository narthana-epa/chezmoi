#!/usr/bin/env fish

function npm --wraps=npm
    set -x NPM_TOKEN (pass API/npm/token)
    command npm $argv
end
