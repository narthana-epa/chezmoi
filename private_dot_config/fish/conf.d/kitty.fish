# kitty
if test $TERM != xterm-kitty
    exit
end

function __kitty_completions
    # Send all words up to the one before the cursor
    commandline -cop | kitty +complete fish
end
complete -f -c kitty -a "(__kitty_completions)"

alias clear "printf '\033[2J\033[3J\033[1;1H'"
alias ssh "kitty +kitten ssh"
